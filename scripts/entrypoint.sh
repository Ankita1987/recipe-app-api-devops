# !bin/bash
set -e

python manage.py collectstatic --noiput
python manage.py wait_for_db
python manage.py migrate

uwsgi --socket :9000 --workers 4 -master --enable-threads --module app.wsqi